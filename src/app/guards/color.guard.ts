import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColorGuard implements CanActivate { //implementa esta interface

  constructor(private router: Router){ //primero viene al constructor, despues va a l canActive

  }

  canActivate( //no se toca
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      this.router.navigate(['home']);
    return false;
  }
  
}
